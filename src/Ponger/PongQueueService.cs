﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Text;
using System.Threading;

namespace Ponger
{
    public class PongQueueService : IDisposable
    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScoper;
        public PongQueueService(IMessageProducerScopeFactory messageProducerScopeFactory, IMessageConsumerScoperFactory messageConsumerScoperFactory)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "ping_queue",
                RoutingKey = "ping_queue"
            });

            _messageConsumerScoper = messageConsumerScoperFactory.Connect(new MessageScopeSettings
            {
                ExangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "pong_queue ",
                RoutingKey = "pong_queue"
            });

            _messageConsumerScoper.MessageConsumer.Received += ListenQueue;
        }

        public void Dispose()
        {
            _messageConsumerScoper?.Dispose();
            _messageProducerScope?.Dispose();
        }

        private void ListenQueue(object sender, BasicDeliverEventArgs args)
        {
            var processed = false;
            try
            {
                var value = Encoding.UTF8.GetString(args.Body);
                Console.WriteLine($"{DateTime.Now.Second} {value}");
                Thread.Sleep(2500);
                SendAnswered("pong");
                processed = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                processed = false;
            }
            finally
            {
                _messageConsumerScoper.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
            }
        }
        private void SendAnswered(string receivedValue)
        {
            _messageProducerScope.MessageProducer.Send(receivedValue);
        }
    }
}
