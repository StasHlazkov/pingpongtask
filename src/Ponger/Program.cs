﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;
using System.Threading;
using customRabbitConnectionFactory = RabbitMQ.Wrapper.QueueServices;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            var uri = new Uri("amqp://guest:guest@localhost:5672/CUSTOM_HOST");
            var connectionfactory = new customRabbitConnectionFactory.ConnectionFactory(uri);

            using (var pongQueueService = new PongQueueService(
                new MessageProducerScopeFactory(connectionfactory),
                new MessageConsumerScoperFactory(connectionfactory)))
            {
                while (true)
                {

                }
            }
        }
    }
}
