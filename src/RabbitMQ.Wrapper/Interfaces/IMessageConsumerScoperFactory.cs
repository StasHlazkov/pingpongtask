﻿using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageConsumerScoperFactory
    {
        IMessageConsumerScope Open(MessageScopeSettings messageScopeSettings);
        IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings);
    }
}
