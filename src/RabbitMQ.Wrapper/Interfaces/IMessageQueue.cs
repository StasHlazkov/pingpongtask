﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageQueue : IDisposable
    {
        IModel Channel { get; init; }
        void BindQueue(string exangeName, string routingKey, string queueName);
        void DeclareExchange(string exangeName, string exchangeType);
    }
}
