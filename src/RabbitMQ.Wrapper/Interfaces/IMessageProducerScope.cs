﻿using System;

namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IMessageProducerScope : IDisposable
    {
        IMessageProducer MessageProducer { get; }
        IMessageQueue CreateMessageQueue();
        IMessageProducer CreateMessageProducer();
    }
}
