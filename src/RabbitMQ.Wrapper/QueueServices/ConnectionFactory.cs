﻿using RabbitMQ.Client;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class ConnectionFactory : RabbitMQ.Client.ConnectionFactory
    {
        public ConnectionFactory(Uri uri)
        {
            Uri = uri;
            RequestedConnectionTimeout = 30000;
            NetworkRecoveryInterval = TimeSpan.FromSeconds(30);
            AutomaticRecoveryEnabled = true;
            TopologyRecoveryEnabled = true;
            RequestedHeartbeat = 60;
        }

        public override IConnection CreateConnection()
        {
            return base.CreateConnection();
        }
    }
}
