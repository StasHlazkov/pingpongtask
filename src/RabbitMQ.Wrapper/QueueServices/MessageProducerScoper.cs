﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageProducerScoper : IMessageProducerScope
    {
        private readonly MessageScopeSettings _messageScopeSettings;
        private readonly Lazy<IMessageQueue> _messageQueueLazy;
        private readonly Lazy<IMessageProducer> _messageProducerLazy;

        private readonly IConnectionFactory _connectionFactory;

        public MessageProducerScoper(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            _messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
            _messageProducerLazy = new Lazy<IMessageProducer>(CreateMessageProducer);
        }

        public IMessageProducer MessageProducer 
        {
            get
            {
                if (!_messageProducerLazy.IsValueCreated)
                {
                    return CreateMessageProducer();
                }
                return _messageProducerLazy.Value;
            }
        }
        private IMessageQueue MessageQueue
        {
            get
            {
                if (!_messageQueueLazy.IsValueCreated)
                {
                    return CreateMessageQueue();
                }
                return _messageQueueLazy.Value;
            }
        }
        public IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSettings);
        }
        public IMessageProducer CreateMessageProducer()
        {
            return new MessageProducer(new MessageProducerSettings
            {
                Channel = MessageQueue.Channel,
                PublicationAddress = new PublicationAddress(
                    _messageScopeSettings.ExchangeType,
                    _messageScopeSettings.ExangeName,
                    _messageScopeSettings.RoutingKey)
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
