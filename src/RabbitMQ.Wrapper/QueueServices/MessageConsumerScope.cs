﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageConsumerScope : IMessageConsumerScope
    {
        private readonly MessageScopeSettings _messageScopeSettings ;
        private  IMessageQueue _messageQueueLazy;
        //private readonly IMessageQueue _messageQueueLazy;
        private  IMessageConsumer _messageConsumerLazy;

        private readonly IConnectionFactory _connectionFactory;

        public MessageConsumerScope(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings)
        {
            _connectionFactory = connectionFactory;
            _messageScopeSettings = messageScopeSettings;

            //_messageQueueLazy = new Lazy<IMessageQueue>(CreateMessageQueue);
            //_messageConsumerLazy = new Lazy<IMessageConsumer>(CreateMessageCosumer);
        }

        public IMessageConsumer MessageConsumer
        {
            get
            {
                if (_messageConsumerLazy == null)
                {
                    _messageConsumerLazy = CreateMessageCosumer();
                }
                return _messageConsumerLazy;
            }
        }

        private IMessageQueue MessageQueue
        {
            get
            {
                if (_messageQueueLazy == null)
                {
                    _messageQueueLazy = CreateMessageQueue();
                }
                return _messageQueueLazy;
            }
        }

        public IMessageQueue CreateMessageQueue()
        {
            return new MessageQueue(_connectionFactory, _messageScopeSettings);
        }
        public IMessageConsumer CreateMessageCosumer()
        {
            return new MessageConsumer(new MessageConsumerSettings
            {
                Channel = MessageQueue.Channel,
                QueueName = _messageScopeSettings.QueueName
            });
        }

        public void Dispose()
        {
            MessageQueue?.Dispose();
        }
    }
}
