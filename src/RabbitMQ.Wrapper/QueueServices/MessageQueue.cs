﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.QueueServices
{
    public class MessageQueue : IMessageQueue
    {
        private readonly IConnection _connection;

        public MessageQueue(IConnectionFactory connectionFactory)
        {
            _connection = connectionFactory.CreateConnection();
            Channel = _connection.CreateModel();
        }

        public MessageQueue(IConnectionFactory connectionFactory, MessageScopeSettings messageScopeSettings) : this(connectionFactory)
        {
            DeclareExchange(messageScopeSettings.ExangeName, messageScopeSettings.ExchangeType);

            if(messageScopeSettings.QueueName != null)
            {
                BindQueue(messageScopeSettings.ExangeName, messageScopeSettings.RoutingKey, messageScopeSettings.QueueName);
            }
        }

        public IModel Channel { get; init; }

        public void BindQueue(string exangeName, string routingKey, string queueName)
        {
            Channel.QueueDeclare(queueName, true, false, false);
            Channel.QueueBind(queueName, exangeName, routingKey);
        }

        public void DeclareExchange(string exangeName, string exchangeType)
        {
            Channel.ExchangeDeclare(exangeName, exchangeType ?? string.Empty);
        }

        public void Dispose()
        {
            Channel?.Dispose();
            _connection?.Dispose();
        }
    }
}
