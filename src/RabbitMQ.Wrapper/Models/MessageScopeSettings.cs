﻿namespace RabbitMQ.Wrapper.Models
{
    public class MessageScopeSettings
    {
        public string ExangeName { get; set; }
        public string QueueName { get; set; }
        public string RoutingKey { get; set; }
        public string ExchangeType { get; set; }

    }
}
