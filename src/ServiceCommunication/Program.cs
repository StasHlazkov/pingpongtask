﻿using RabbitMQ.Client;
using RabbitMQ.Wrapper.QueueServices;
using System;
using System.Text;
using System.Threading;
using customRabbitConnectionFactory = RabbitMQ.Wrapper.QueueServices;

namespace Pinger
{
    class Program
    {

        static void Main(string[] args)
        {
            var uri = new Uri("amqp://guest:guest@localhost:5672/CUSTOM_HOST");
            var connectionfactory = new customRabbitConnectionFactory.ConnectionFactory(uri);

            using (var pingQueueService = new PingQueueService(
                new MessageProducerScopeFactory(connectionfactory),
                new MessageConsumerScoperFactory(connectionfactory)))
            {
                pingQueueService.SendMessageToQueue("ping");
                while (true) 
                {
                }

            }
        }
    }
}

