﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;
using System;
using System.Text;
using System.Threading;

namespace Pinger
{
    public class PingQueueService : IDisposable
    {
        private readonly IMessageProducerScope _messageProducerScope;
        private readonly IMessageConsumerScope _messageConsumerScoper;
        public PingQueueService(IMessageProducerScopeFactory messageProducerScopeFactory, IMessageConsumerScoperFactory messageConsumerScoperFactory)
        {
            _messageProducerScope = messageProducerScopeFactory.Open(new MessageScopeSettings
            {
                ExangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "pong_queue",
                RoutingKey = "pong_queue"
            });

            _messageConsumerScoper = messageConsumerScoperFactory.Connect(new MessageScopeSettings
            {
                ExangeName = "ServerExchange",
                ExchangeType = ExchangeType.Topic,
                QueueName = "ping_queue",
                RoutingKey = "ping_queue"
            });

            _messageConsumerScoper.MessageConsumer.Received += ListenQueue;
        }

        public void Dispose()
        {
            _messageConsumerScoper?.Dispose();
            _messageProducerScope?.Dispose();
        }

        public bool SendMessageToQueue(string value)
        {
            try
            {
                _messageProducerScope.MessageProducer.Send(value);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        private void ListenQueue(object sender, BasicDeliverEventArgs args)
        {
            var processed = false;
            try
            {
                var value = Encoding.UTF8.GetString(args.Body);
                Console.WriteLine($"{DateTime.Now.Second} {value}");
                Thread.Sleep(2500);
                SendAnswered("ping");
                processed = true;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                processed = false;
            }
            finally
            {
                _messageConsumerScoper.MessageConsumer.SetAcknowledge(args.DeliveryTag, processed);
            }
        }
        private void SendAnswered(string receivedValue)
        {
            _messageProducerScope.MessageProducer.Send(receivedValue);
        }
    }
}
